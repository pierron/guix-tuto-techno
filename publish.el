;; Transforms Org documents in the repository into a HTML website and exports
;; selected documents to PDF.
;;
;; Based on the work of Rasmus.

(require 'org) ;; Org mode support
(require 'htmlize) ;; source code block export to HTML
(require 'ess) ;; evalutation of R source code blocks
(require 'ox-publish) ;; publishing functions
(require 'ox-latex) ;; LaTeX publishing functions
(require 'org-ref) ;; bibliography support

;; Define a function for selective content export based on the backend.
(defun filter-export (backend)
  "Remove every headline with ':htmlonly:' tag from the tree when exporting to
LaTeX. BACKEND provides the export back-end being used."
  (if (org-export-derived-backend-p backend 'latex)
      (org-map-entries '(org-toggle-tag "noexport" "on") "+htmlonly")))

;; Add the selective export filter function as a hook.
(add-hook 'org-export-before-parsing-hook 'filter-export)

;; Force publishing of unchanged files to make sure all the pages get published.
;; Otherwise, the files considered unmodified based on Org timestamps are not
;; published even if they were previously deleted from the publishing directory.
(setq org-publish-use-timestamps-flag nil)

;; Preserve user-defined labels during the export to PDF via LaTeX.
(setq org-latex-prefer-user-labels t)

;; Override the default LaTeX publishing command.
(setq org-latex-pdf-process (list "latexmk --shell-escape -f -pdf %f"))

;; Load languages for code block evaluation.
(org-babel-do-load-languages
 'org-babel-load-languages
 '((shell . t)
   (R . t)))

;; Do not prompt for code block evaluation.
(setq org-confirm-babel-evaluate nil)

;; Enable Babel code evaluation.
(setq org-export-babel-evaluate t)

;; Preserve indentation on export and tangle.
(setq org-src-preserve-indentation t)

;; Use TAB to indent code in org mode code blocks.
(setq org-src-tab-acts-natively t)

;; Save global preferences for the appearance of code blocks.
(setq my-minted-options
      '(("linenos = false") ("mathescape") ("breaklines")
        ("bgcolor = yellow!15")))

;; Configure LaTeX to use 'minted' for code block export.
(setq org-latex-packages-alist '())
(add-to-list 'org-latex-packages-alist '("" "minted"))
(setq org-latex-listings 'minted)
(setq org-latex-minted-options my-minted-options)

;; Put table captions below the tables rather than above in the LaTeX export.
(setq org-latex-caption-above nil)

(defun org-babel-execute-file (plist filename pub-dir)
  ;; A custom publishing function for executing all Babel source code blocks in
  ;; the Org file `filename'. Note that the function does not actually publish
  ;; anything to `pub-dir'.
  (progn
    (find-file filename)
    (org-babel-execute-buffer)
    (set-buffer-modified-p nil)
    (kill-buffer)))

;; Include the Inria favicon in to the HTML header.
(setq org-html-head-extra "<link rel=\"icon\" type=\"image/x-icon\"
href=\"https://mfelsoci.gitlabpages.inria.fr/thesis/favicon.ico\"/>")

;; Do not wrap inline code evaluation results into verbatim environment.
(setq org-babel-inline-result-wrap "%s")

;; Configure HTML website and PDF document publishing.
(setq org-publish-project-alist
      (list
       (list "site"
             :base-directory "."
             :exclude ".*"
             :include ["reproducing-guidelines.org" "study-article.org"]
             :base-extension "org"
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public")
       (list "article"
             :base-directory "."
             :exclude ".*"
             :include ["study-article.org"]
             :base-extension "org"
             :publishing-function '(org-latex-publish-to-pdf)
             :publishing-directory "./public")
       (list "RR"
             :base-directory "."
             :exclude ".*"
             :include ["study-research-report.org"]
             :base-extension "org"
             :publishing-function '(org-latex-publish-to-pdf)
             :publishing-directory "./public")
       (list "RT"
             :base-directory "."
             :exclude ".*"
             :include ["reproducing-guidelines.org"]
             :base-extension "org"
             :publishing-function '(org-latex-publish-to-pdf)
             :publishing-directory "./public")
       (list "figures"
             :base-directory "./figures"
             :base-extension "png\\|jpg\\|gif\\|svg\\|ico"
             :recursive t
             :publishing-directory "./public/figures"
             :publishing-function '(org-publish-attachment))
       (list "manuscripts"
             :components '("article" "RR" "RT"))
       (list "study"
             :components '("site" "manuscripts"))
       (list "study-static"
             :components '("figures"))))

(provide 'publish)
