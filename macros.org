#+MACRO: test_FEMBEM ~test_FEMBEM~
#+MACRO: hmat HMAT
#+MACRO: hmat-oss HMAT-OSS
#+MACRO: chameleon Chameleon
#+MACRO: epsilon 10^{-3}
#+MACRO: spipe /short pipe/
#+MACRO: hmatrix \(\mathcal{H}\)-Matrix
#+MACRO: ggplot ~ggplot2~
#+MACRO: svglite ~svglite~
